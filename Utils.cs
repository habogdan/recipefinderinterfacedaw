﻿using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeFinder
{
    public class Utils
    {

        public static async Task<string> GetAccountTypeAsync( string accessToken)
        {
            
            var jwtToken = new JwtSecurityToken(accessToken);
            var permissions = jwtToken.Claims.Where(c => c.Type == "permissions").Select(c => c).ToList();
            if (permissions.FirstOrDefault(p => p.Value == "write:recipe") != null)
            {
                return "administrator";
            }
            else if (permissions.FirstOrDefault(p => p.Value == "save:recipe") != null)
            {
                return "user";
            }
            else
            {
                return "guest";
            }
        }
    }
}
