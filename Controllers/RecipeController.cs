﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting.Internal;
using Newtonsoft.Json;
using RecipeFinder.Models;
using RestSharp;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RecipeFinder.Controllers
{
    public class RecipeController : Controller
    {
        public IHostingEnvironment _hostingEnvironment;
        public RecipeController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }


        // GET: /<controller>/
        public async Task<IActionResult> Test()
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");

            var client = new RestClient("http://localhost:5000/api/recipes/1");
            var request = new RestRequest(Method.GET);
            request.AddHeader("authorization", "Bearer " + accessToken);
            IRestResponse response = client.Execute(request);

            return View();
        }

        public async Task<IActionResult> Show(int id)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var sendID = new Dictionary<string, string>();
            sendID.Add("access_token" , accessToken);
            sendID.Add("id", id.ToString());
            ViewData["account_type"] = await Utils.GetAccountTypeAsync(accessToken);
            return View(sendID);
        }

        public IActionResult CreateRecipe()
        {
            var model = new RecipeViewModel();
            return View(model);
        }

        public IActionResult AddIngredient()
        {
            var model = new IngredientViewModel();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateRecipe(RecipeViewModel obItem)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "img");
            FileUploadHelper objFile = new FileUploadHelper();
            string strFilePath = await objFile.SaveFileAsync(obItem.formFile, uploads);
            strFilePath = strFilePath
             .Replace(_hostingEnvironment.WebRootPath, string.Empty)
             .Replace("\\", "/"); //Relative Path can be stored in database or do logically what is needed.

            var client = new RestClient("http://localhost:5000/api/Recipes/");
            var request = new RestRequest(Method.POST);
            request.AddHeader("authorization", "Bearer " + accessToken);
            request.AddJsonBody(new
            {
                Name = obItem.Name,
                Description = obItem.Description,
                AccountID = 1,
                PhotoName = strFilePath.Split('/').Last()
            });
            IRestResponse response = client.Execute(request);

           

            return this.RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public async Task<IActionResult> DeleteRecipe( int id)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var client = new RestClient("http://localhost:5000/api/Recipes/" + id);
            var request = new RestRequest(Method.DELETE);
            request.AddHeader("authorization", "Bearer " + accessToken);
            IRestResponse response = client.Execute(request);

            return this.RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var client = new RestClient("http://localhost:5000/api/Recipes/" + id);
            var request = new RestRequest(Method.GET);
            request.AddHeader("authorization", "Bearer " + accessToken);
            var response = client.Execute(request);
            var obj = JsonConvert.DeserializeObject<RecipeViewModel>(response.Content); 

            return View(obj);
        }

        public async Task<IActionResult> Edit(RecipeViewModel obItem)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var client = new RestClient("http://localhost:5000/api/Recipes/" + obItem.id);
            var request = new RestRequest(Method.PUT);
            request.AddHeader("authorization", "Bearer " + accessToken);

            if(obItem.formFile != null)
            {
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "img");
                FileUploadHelper objFile = new FileUploadHelper();
                string strFilePath = await objFile.SaveFileAsync(obItem.formFile, uploads);
                strFilePath = strFilePath
                 .Replace(_hostingEnvironment.WebRootPath, string.Empty)
                 .Replace("\\", "/"); //Relative Path can be stored in database or do logically what is needed.

                obItem.PhotoName = strFilePath.Split('/').Last();
            }

            request.AddJsonBody(new
            {
                Name = obItem.Name,
                Description = obItem.Description,
                AccountID = 1,
                PhotoName = obItem.PhotoName
            });
            IRestResponse response = client.Execute(request);

            return this.RedirectToAction("Index", "Home");

        }
    }
}
