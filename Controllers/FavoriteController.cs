﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeFinder.Controllers
{
    public class FavoriteController : Controller
    {

        public async Task<IActionResult> Add(string userID, int recipeID)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var client = new RestClient("http://localhost:5000/api/Recipes/Favorites/"+ recipeID + "/" + userID);
            var request = new RestRequest(Method.POST);
            request.AddHeader("authorization", "Bearer " + accessToken);
            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                return this.RedirectToAction("Index", "Home");
            }
            else
            {
                return this.RedirectToAction("Error", "Home");
            }

            
        }

        public async Task<IActionResult> Remove(string userID, int recipeID)
        {
            string accessToken = await HttpContext.GetTokenAsync("access_token");
            var client = new RestClient("http://localhost:5000/api/Recipes/Favorites/" + recipeID + "/" + userID);
            var request = new RestRequest(Method.DELETE);
            request.AddHeader("authorization", "Bearer " + accessToken);
            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                return this.RedirectToAction("Index", "Home");
            }
            else
            {
                return this.RedirectToAction("Error", "Home");
            }


        }


    }
}
