﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RecipeFinder.Models;
using RestSharp;

namespace RecipeFinder.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> IndexAsync()
        {
            var Favorites = new List<int>();

            if (User.Identity.IsAuthenticated)
            {
                string accessToken = await HttpContext.GetTokenAsync("access_token");
                ViewData["account_type"] = await Utils.GetAccountTypeAsync(accessToken);
                ViewData["account_id"] = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

                var client = new RestClient("http://localhost:5000/api/Recipes/Favorites/"+ViewData["account_id"]);
                var request = new RestRequest(Method.GET);
                request.AddHeader("authorization", "Bearer " + accessToken);
                var response = client.Execute(request);
                var favorites = JsonConvert.DeserializeObject<IEnumerable<Dictionary<string,string>>>(response.Content);
                foreach(var favorite in favorites)
                {
                    Favorites.Add(Int32.Parse(favorite["recipeID"]));
                }
            }
            return View(Favorites);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
