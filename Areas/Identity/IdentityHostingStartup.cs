﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RecipeFinder.Models;

[assembly: HostingStartup(typeof(RecipeFinder.Areas.Identity.IdentityHostingStartup))]
namespace RecipeFinder.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<RecipeFinderContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("RecipeFinderContextConnection")));

                services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<RecipeFinderContext>();
            });
        }
    }
}